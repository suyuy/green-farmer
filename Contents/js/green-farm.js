//auto width search box
$('#box').focus(function()
{
    /*to make this flexible, I'm storing the current width in an attribute*/
    $(this).attr('data-default', $(this).width());
    $(this).animate({ width: 250 }, 'slow');
}).blur(function()
{
    /* lookup the original width */
    var w = $(this).attr('data-default');
    $(this).animate({ width: 80  }, 'slow');
});
//end auto widht 

function hide(obj){
	document.getElementById(obj).style.visibility = "hidden";
}

function show(obj){
	document.getElementById(obj).style.visibility= "visible";
}